package org.springboot.thyemeleaf.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/thyemeleaf")
public class ThyemeleafController {

	@RequestMapping("/index")
    public ModelAndView regist(){
        ModelAndView view = new ModelAndView("index");
        view.addObject("name", "哈哈哈");
        view.addObject("age", "25");
        return view;
    }
	
}
