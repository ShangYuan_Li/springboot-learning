package org.springboot.rabbitmq.listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class QueueListener {

	@RabbitListener(queues = "spring-boot", containerFactory = "mainRabbitListenerContainerFactory")
    public void springBootQueue(String msg) {
        System.out.println("springBootQueue receive message: " + msg);
    }
	
	@RabbitListener(queues = "direct-1", containerFactory = "mainRabbitListenerContainerFactory")
    public void directQueue1(String msg) {
        System.out.println("directQueue1 receive message: " + msg);
    }
	
	@RabbitListener(queues = "direct-2", containerFactory = "mainRabbitListenerContainerFactory")
    public void directQueue2(String msg) {
        System.out.println("directQueue2 receive message: " + msg);
    }
	
	@RabbitListener(queues = "fanout-1", containerFactory = "mainRabbitListenerContainerFactory")
    public void fanoutQueue1(String msg) {
        System.out.println("fanoutQueue1 receive message: " + msg);
    }
	
	@RabbitListener(queues = "fanout-2", containerFactory = "mainRabbitListenerContainerFactory")
    public void fanoutQueue2(String msg) {
        System.out.println("fanoutQueue2 receive message: " + msg);
    }
	
	@RabbitListener(queues = "topic-1", containerFactory = "mainRabbitListenerContainerFactory")
    public void topicQueue1(String msg) {
        System.out.println("topicQueue1 receive message: " + msg);
    }
	
	@RabbitListener(queues = "topic-2", containerFactory = "mainRabbitListenerContainerFactory")
    public void topicQueue2(String msg) {
        System.out.println("topicQueue2 receive message: " + msg);
    }
}
