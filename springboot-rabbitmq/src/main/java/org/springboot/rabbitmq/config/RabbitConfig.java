package org.springboot.rabbitmq.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class RabbitConfig {

	@Bean("mainConnectionFactory")
	@Primary
    public ConnectionFactory mainConnectionFactory(
            @Value("${spring.rabbitmq.main.host}") String host,
            @Value("${spring.rabbitmq.main.port}") int port,
            @Value("${spring.rabbitmq.main.username}") String username,
            @Value("${spring.rabbitmq.main.password}") String password,
            @Value("${spring.rabbitmq.main.virtualhost}") String virtualHost) {
        return connectionFactory(host, port, username, password, virtualHost);
    }

	public CachingConnectionFactory connectionFactory(String host, int port, String username, String password, String virtualHost) {
		// 连接方式一
		/*CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host);
		connectionFactory.setUsername(username);
		connectionFactory.setPassword(password);
		connectionFactory.setPort(port);
		connectionFactory.setVirtualHost("virtualHost");*/
		// 连接方式二
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
		String uri = "amqp://" + username + ":" + password + "@" + host + ":" + port + virtualHost;
		connectionFactory.setUri(uri);	
		return connectionFactory;
	}

	@Bean("mainAmqpAdmin")
	@Primary
	public AmqpAdmin mainAmqpAdmin(@Qualifier("mainConnectionFactory") ConnectionFactory connectionFactory) {
		return new RabbitAdmin(connectionFactory);
	}

    @Bean("mainRabbitTemplate")
    @Primary
    public RabbitTemplate mainRabbitTemplate(@Qualifier("mainConnectionFactory") ConnectionFactory connectionFactory) {
        RabbitTemplate mainRabbitTemplate = new RabbitTemplate(connectionFactory);
        return mainRabbitTemplate;
    }

	@Bean("mainRabbitListenerContainerFactory")
	@Primary
	public SimpleRabbitListenerContainerFactory mainRabbitListenerContainerFactory(@Qualifier("mainConnectionFactory") ConnectionFactory connectionFactory) {
		// 配置消费者监听的容器
		SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory);
		factory.setConcurrentConsumers(3);
		factory.setMaxConcurrentConsumers(10);
		// 设置确认模式手工确认
		// factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);
		return factory;
	}
}
