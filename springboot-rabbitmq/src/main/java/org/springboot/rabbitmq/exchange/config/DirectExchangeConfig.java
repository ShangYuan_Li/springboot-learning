package org.springboot.rabbitmq.exchange.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * direct 直连模式的交换机配置
 * 	- 一个 direct 交换机
 * 	- 两个队列
 * 	- 三根网线 binding
 * 
 * @author lisy
 */
@Configuration
public class DirectExchangeConfig {

	/**
	 * 直连型交换机(direct exchange): 
	 *   是根据消息携带的路由键(routing key)将消息投递给对应队列的
	 * 工作步骤:
	 * 1. 将一个队列绑定到某个交换机上，同时赋予该绑定一个路由键(routing key)
	 * 2. 当一个携带着路由键为 R 的消息被发送给直连交换机时，交换机会把它路由给绑定值同样为 R 的队列
	 */
	
    @Bean
    public DirectExchange directExchange() {
        DirectExchange directExchange = new DirectExchange("direct");
        return directExchange;
    }

    @Bean
    public Queue directQueue1() {
        Queue queue = new Queue("direct-1");
        return queue;
    }

    @Bean
    public Queue directQueue2() {
        Queue queue = new Queue("direct-2");
        return queue;
    }

    // 3 个 binding 将交换机和相应队列连起来
    @Bean
    public Binding bindingorange() {
        Binding binding = BindingBuilder.bind(directQueue1()).to(directExchange()).with("orange");
        return binding;
    }

    @Bean
    public Binding bindingblack() {
        Binding binding = BindingBuilder.bind(directQueue2()).to(directExchange()).with("black");
        return binding;
    }

    @Bean
    public Binding bindinggreen() {
        Binding binding = BindingBuilder.bind(directQueue2()).to(directExchange()).with("green");
        return binding;
    }
}
