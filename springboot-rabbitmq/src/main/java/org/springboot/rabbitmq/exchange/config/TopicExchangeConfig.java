package org.springboot.rabbitmq.exchange.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * topic 交换机模型
 * 	- 一个 topic 交换机
 * 	- 两个队列
 * 	- 三个 binding
 * 
 * @author lisy
 */
@Configuration
public class TopicExchangeConfig {

	/**
	 * 主题交换机(topic exchanges): 
	 *   通过对消息的路由键和队列到交换机的绑定模式之间的匹配，将消息路由给一个或多个队列。主题交换机经常用来实现各种
	 * 分发/订阅模式及其变种。主题交换机通常用来实现消息的多播路由(multicast routing)
	 */

	@Bean
	public TopicExchange topicExchange() {
		TopicExchange topicExchange = new TopicExchange("topic");
		return topicExchange;
	}

	@Bean
	public Queue topicQueue1() {
		Queue queue = new Queue("topic-1");
		return queue;
	}

	@Bean
	public Queue topicQueue2() {
		Queue queue = new Queue("topic-2");
		return queue;
	}

	/**
	 * 3 个 binding 将交换机和相应队列连起来
	 */

	@Bean
	public Binding bindingtopic1() {
		// binding key
		Binding binding = BindingBuilder.bind(topicQueue1()).to(topicExchange()).with("*.orange.*");
		return binding;
	}

	@Bean
	public Binding bindingtopic2() {
		Binding binding = BindingBuilder.bind(topicQueue2()).to(topicExchange()).with("*.*.rabbit");
		return binding;
	}

	@Bean
	public Binding bindingtopic3() {
		// # 表示 0 个或若干个关键字，* 表示一个关键字
		Binding binding = BindingBuilder.bind(topicQueue2()).to(topicExchange()).with("lazy.#");
		return binding;
	}
}
