package org.springboot.rabbitmq.exchange.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 生产者消费者模式的配置
 * 	- 一个队列
 * 	- 多个对应的消费者
 * 
 * @author lisy
 */
@Configuration
public class DefaultExchangeConfig {

	/**
	 * 默认交换机(default exchange):
	 *   实际上是一个由消息代理预先声明好的没有名字（名字为空字符串）的直连交换机(direct exchange)。它有一个特殊的属性使得它对于简单应用
	 * 特别有用处：那就是每个新建队列(queue)都会自动绑定到默认交换机上，绑定的路由键(routing key)名称与队列名称相同
	 */
	@Bean
    public Queue queue() {
        Queue queue = new Queue("spring-boot");
        return queue;
    }
}
