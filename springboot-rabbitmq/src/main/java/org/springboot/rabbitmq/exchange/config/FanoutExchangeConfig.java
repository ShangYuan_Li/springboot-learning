package org.springboot.rabbitmq.exchange.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 发布订阅模式的配置
 * 	- 两个队列和对应的订阅者
 * 	- 发布者的交换机类型使用 fanout (子网广播)
 * 	- 两根网线 binding 用来绑定队列到交换机
 * 
 * @author lisy
 */
@Configuration
public class FanoutExchangeConfig {

	/**
	 * 扇型交换机(funout exchange):
	 *   将消息路由给绑定到它身上的所有队列，而不理会绑定的路由键。如果 N 个队列绑定到某个扇型交换机上，当有消息发送给此扇型交换机时，
	 * 交换机会将消息的拷贝分别发送给这所有的 N 个队列。扇型用来交换机处理消息的广播路由(broadcast routing)
	 */

    @Bean
    public FanoutExchange fanoutExchange() {
        FanoutExchange fanoutExchange = new FanoutExchange("fanout");
        return fanoutExchange;
    }

    @Bean
    public Queue fanout1() {
        Queue queue = new Queue("fanout-1");
        return queue;
    }

    @Bean
    public Queue fanout2() {
        Queue queue = new Queue("fanout-2");
        return queue;
    }

    @Bean
    public Binding binding1() {
        Binding binding = BindingBuilder.bind(fanout1()).to(fanoutExchange());
        return binding;
    }

    @Bean
    public Binding binding2() {
        Binding binding = BindingBuilder.bind(fanout2()).to(fanoutExchange());
        return binding;
    }
}
