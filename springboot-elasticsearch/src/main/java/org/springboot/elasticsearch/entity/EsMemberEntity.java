package org.springboot.elasticsearch.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

/**
 * @Document标记该实体类为es的实体对象: indexName属性指明在哪个索引下
 * @author lisy
 */
@Document(indexName = "member")
public class EsMemberEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * @Field 标记该变量对应该表的某个字段,其中的属性有:
	 *  - index：是否对该字段分词，默认true
	 *  - analyzer：指定插入时的分词模式，一般为ik_max_words
	 *  - type：字段类型
	 *  - store：是否存储该字段，默认值为false
	 *  - searchAnalyzer：指定查询时的分词模式，一般为ik_smart
	 * IK中有2种分词模式:
	 * 	- ik_max_words 最细粒度分词
	 *  - ik_smart 智能分词
	 */
	@Field(name = "id")
	private int id;
	
	@Field(name = "name")
    private String name;
	
    @Field(name = "time")
    private Date time;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
}
