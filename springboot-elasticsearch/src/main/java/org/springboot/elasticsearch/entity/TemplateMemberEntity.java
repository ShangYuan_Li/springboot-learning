package org.springboot.elasticsearch.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @Document标记该实体类为es的实体对象: indexName属性指明在哪个索引下
 * @author lisy
 */
@Document(indexName = "tmpl_member")
public class TemplateMemberEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Field(name = "name", type = FieldType.Text)
    private String name;
	
	@Field(name = "age", type = FieldType.Integer)
	private int age;
	
    @Field(name = "time", type = FieldType.Date)
    private Date time;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
}
