package org.springboot.elasticsearch.config;

import java.time.Duration;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;

@Configuration
public class ElasticSearchConfig {

	@Value("${elastic-search.host.port}")
	private String hostAndPort;
	// socket超时时间
	@Value("${elastic-search.socket-timeout}")
	private String socketTimeout;
	// 连接超时时间
	@Value("${elastic-search.connection-timeout}")
	private String connectionTimeout;

	@Bean
	public RestHighLevelClient restHighLevelClient() {
		final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
				.connectedTo(hostAndPort)
				.withSocketTimeout(Duration.ofSeconds(Long.parseLong(socketTimeout)))
				.withConnectTimeout(Long.parseLong(connectionTimeout))
				.build();
		RestHighLevelClient restHighLevelClient = RestClients.create(clientConfiguration).rest();
		return restHighLevelClient;
	}

}
