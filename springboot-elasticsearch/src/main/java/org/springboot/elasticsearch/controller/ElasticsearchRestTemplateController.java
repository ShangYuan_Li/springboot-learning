package org.springboot.elasticsearch.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springboot.elasticsearch.entity.TemplateMemberEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.IndexInformation;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/es/elasticsearchRestTemplate")
public class ElasticsearchRestTemplateController {

	static Logger logger = LoggerFactory.getLogger(ElasticsearchRestTemplateController.class);
	
	@Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;
	
	/**
	 * 创建索引
	 * @return
	 */
	@RequestMapping("/createIndex")
	public Object createIndex() {
		Map<String, Object> map = new HashMap<String, Object>();
		IndexCoordinates indexCoordinates = IndexCoordinates.of("tmpl_member");
		if (!elasticsearchRestTemplate.indexOps(indexCoordinates).exists()) {
			Map<String, Object> settings = new HashMap<String, Object>();
			settings.put("index", new HashMap<String, Object>() {
				private static final long serialVersionUID = 1L;
				{
					put("number_of_shards", "1");
					put("number_of_replicas", "1");
				}
			});
			// 创建索引和settings
			boolean create = elasticsearchRestTemplate.indexOps(indexCoordinates).create(settings);
			map.put("create", create);
			// 创建索引mapping
            boolean mapping = elasticsearchRestTemplate.indexOps(indexCoordinates).putMapping(TemplateMemberEntity.class);
            map.put("mapping", mapping);
		} else {
			map.put("msg", "index already exists!");
		}
		map.put("state", "success");
		return map;
	}
	
	/**
	 * 查询索引信息
	 * @param index
	 * @return
	 */
	@RequestMapping("/getIndexInfo")
	public Object getIndexInfo(String index){
		IndexCoordinates indexCoordinates = IndexCoordinates.of(index);
		List<IndexInformation> information = null;
		try {
			information = elasticsearchRestTemplate.indexOps(indexCoordinates).getInformation();
			return information;
		} catch (Exception e) {
			logger.error("获取索引" + index + "信息失败！", e);
			return e.toString();
		}
	}
	
	/**
	 * 删除索引信息
	 * @param index
	 * @return
	 */
	@RequestMapping("/deleteIndex")
	public Object deleteIndex(String index) {
		boolean delete = elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).delete();
		return delete;
	}
	
	/**
	 * 根据索引的文档ID新增数据
	 * @param index
	 * @param indexId
	 * @param id
	 * @param name
	 * @return
	 */
	@RequestMapping("/insertDataToIndex")
	public Object insertDataToIndex(String index, @RequestParam(value = "index_id") String indexId, String name, int age) {
		TemplateMemberEntity member = new TemplateMemberEntity();
		member.setName(name);
		member.setAge(age);
		member.setTime(new Date());
		IndexQuery indexQuery = new IndexQueryBuilder().withId(indexId).withObject(member).build();
		String result = elasticsearchRestTemplate.index(indexQuery, IndexCoordinates.of(index));
		return result;
	}
}
