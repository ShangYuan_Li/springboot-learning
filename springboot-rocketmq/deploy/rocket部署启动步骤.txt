
1.  配置 ROCKETMQ_HOME 环境变量
2.  启动 NameServer，可以考虑优化启动内存 set "JAVA_OPT=%JAVA_OPT% -server -Xms256m -Xmx512m"
 - NameServer默认启动端口是9876，看到 The Name Server boot success 字样，表示NameServer己启动成功
 方式一 运行 mqnamesrv.cmd 文件
 方式二 start mqnamesrv.cmd
3. 启动 Broker，可以考虑优化启动内存 set “JAVA_OPT=%JAVA_OPT% -server -Xms256m -Xmx512m”
 - 看到 The broker … boot success 字样，表示Broker己启动成功
 方式一 运行 mqbroker.cmd 文件，在 call xxx 之前添加 NameServer 的地址 set "NAMESRV_ADDR=localhost:9876"
 方式二 执行命令 start mqbroker.cmd -n 127.0.0.1:9876 autoCreateTopicEnable=true
4. 启动消费者
 set NAMESRV_ADDR=127.0.0.1:9876
 tools.cmd org.apache.rocketmq.example.quickstart.Consumer
5. 启动生产者
 set NAMESRV_ADDR=127.0.0.1:9876
 tools.cmd org.apache.rocketmq.example.quickstart.Producer
6. 启动成功后，生产者会发送1000个消息，然后自动退出。在消费者界面按下Ctrl + C，就会收到刚刚生产者发出的消息。


rocketmq 控制台 
1. 下载项目 https://github.com/apache/rocketmq-dashboard
2. 修改端口号-8076（默认端口8080 默认使用rabbitmq端口9876）
3. 打包 mvn clean package -Dmaven.test.skip=true
 - 若出现‘yarn install’ failed 错误，把pom.xml中的frontend-maven-plugin和maven-antrun-plugin插件注释掉就能打包成功了
4. 启动 java -jar target/rocketmq-dashboard-1.0.1-SNAPSHOT.jar
 - 控制台地址 http://127.0.0.1:8076/

