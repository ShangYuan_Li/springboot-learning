package org.springboot.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(
		nameServer = "${rocketmq.one.name-server}",
		consumerGroup = "consumerGroupCust",
        topic = "test_boot_cust",
        consumeMode = ConsumeMode.ORDERLY,
        messageModel = MessageModel.CLUSTERING
        )
public class CustConsumerListener implements RocketMQListener<String> {

	private static final Logger logger = LoggerFactory.getLogger(CustConsumerListener.class);

    @Override
    public void onMessage(String message) {
        Thread thread = Thread.currentThread();
        logger.info("线程【ID：" + thread.getId() + "，名称：" + thread.getName() + "】开始执行处理消息监听器.....");
        logger.info(message);
        logger.info("线程【ID：" + thread.getId() + "，名称：" + thread.getName() + "】结束执行处理消息监听器。");
    }

}
