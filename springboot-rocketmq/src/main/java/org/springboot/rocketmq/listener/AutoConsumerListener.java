package org.springboot.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(consumerGroup = "consumerGroupAuto",
        topic = "test_boot_auto", 
        // 无 tag 时可不设置，不设置或为默认值（*）时消费topic下所有消息
        selectorExpression = "*",
        // ConsumeMode.CONCURRENTLY(默认方式，非顺序消费)，ConsumeMode.ORDERLY(顺序消费，单线程)
        consumeMode = ConsumeMode.CONCURRENTLY,
        // MessageModel.CLUSTERING：集群模式（默认），MessageModel.BROADCASTING：广播模式
        messageModel = MessageModel.BROADCASTING
        )
public class AutoConsumerListener implements RocketMQListener<String> {

	private static final Logger logger = LoggerFactory.getLogger(AutoConsumerListener.class);

	/**
	 * 消费模式：
	 * - 负载均衡(MessageModel.CLUSTERING)：多个消费者共同瓜分所有消息，一个消息只能被一个消费者消费掉
	 * - 广播模式(MessageModel.BROADCASTING)：每个消费者都会消费同样的消息。一个消息会被所有消费者消费掉
	 * 过滤消息：
	 * - Tag 生产者发送消息时可以指定 Tag，消费者消费消息可以指定 Tag 过滤，也可以使用通配符 * 表示所有 Tag，也可以使用 || 或来表示多个Tag，使用 Template 发送带 Tag 的消息是使用冒号分隔跟在主题后面
	 * - SQL 基本语法，SQL92 过滤，默认是关闭的，需要在 broker.conf 文件配置 enablePropertyFilter=true，修改配置文件后要根据配置文件启动，如sh ./bin/mqbroker -c ./conf/broker.conf
	 */
	
    @Override
    public void onMessage(String message) {
        Thread thread = Thread.currentThread();
        logger.info("线程【ID：" + thread.getId() + "，名称：" + thread.getName() + "】开始执行处理消息监听器.....");
        logger.info(message);
        logger.info("线程【ID：" + thread.getId() + "，名称：" + thread.getName() + "】结束执行处理消息监听器。");
    }

}
