package org.springboot.rocketmq.config;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RocketMqConfig {

    @Value("${rocketmq.one.name-server}")
    private String oneServer;
    @Value("${rocketmq.one.producer.group}")
    private String oneProducerGroup;

    @Bean("custMQTemplate")
    public RocketMQTemplate custMQTemplate() {
    	DefaultMQProducer producer = new DefaultMQProducer(oneProducerGroup);
        producer.setUnitName("one");
        producer.setNamesrvAddr(oneServer);
        RocketMQTemplate rocketMQTemplate = new RocketMQTemplate();
        rocketMQTemplate.setProducer(producer);
        return rocketMQTemplate;
    }

}
