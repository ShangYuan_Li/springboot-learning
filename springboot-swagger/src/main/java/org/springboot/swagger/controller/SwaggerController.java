package org.springboot.swagger.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags="swagger模块")
@RestController
@RequestMapping("/swagger")
public class SwaggerController {

	@ApiOperation("swagger api test")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name="str", value="请求字符串", dataType="String", required=true)
    })
    @ApiResponses({
    	@ApiResponse(code = 200, message = "请求成功"),
        @ApiResponse(code = 400, message = "请求参数没填好"),
        @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对")
    })
	@RequestMapping(value="/api", method = RequestMethod.POST)
    public String api(String str){
        return str;
    }
}
