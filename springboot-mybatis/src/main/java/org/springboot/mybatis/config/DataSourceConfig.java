package org.springboot.mybatis.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.druid.pool.DruidDataSource;

@Configuration
public class DataSourceConfig {
	
    @Value("${spring.datasource.one.url}")
    private String url;
 
    @Value("${spring.datasource.one.username}")
    private String user;
 
    @Value("${spring.datasource.one.password}")
    private String password;
 
    @Value("${spring.datasource.one.driver-class-name}")
    private String driverClass;

	@Bean(name = "dataSourceOne")
    public DataSource dataSourceOne() {
		DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        
        return dataSource;
    }
	
    @Value("${spring.datasource.two.url}")
    private String tUrl;
 
    @Value("${spring.datasource.two.username}")
    private String tUser;
 
    @Value("${spring.datasource.two.password}")
    private String tPassword;
 
    @Value("${spring.datasource.two.driver-class-name}")
    private String tDriverClass;
	
	@Bean(name = "dataSourceTwo")
    public DataSource dataSourceTwo() {
		DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(tDriverClass);
        dataSource.setUrl(tUrl);
        dataSource.setUsername(tUser);
        dataSource.setPassword(tPassword);
        return dataSource;
    }
}
