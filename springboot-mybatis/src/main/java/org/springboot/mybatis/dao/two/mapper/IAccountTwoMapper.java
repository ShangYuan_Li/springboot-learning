package org.springboot.mybatis.dao.two.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;
import org.springboot.mybatis.dao.entity.Account;

@Mapper
public interface IAccountTwoMapper {

	@Insert("insert into account_two(name, money) values(#{name}, #{money})")
    int add(@Param("name") String name, @Param("money") double money);

    @Update({"<script>",
		"update account_two ",
		"set name = #{name}, money = #{money} ",
		"<where>",
		"id = #{id}",
		"</where>",
		"</script>"
    })
    int update(@Param("name") String name, @Param("money") double money, @Param("id") int  id);

    @Delete("delete from account_two where id = #{id}")
    int delete(int id);

    @Select("<script>"
		+ "select id, name as name, money as money "
		+ "from account_two "
		+ "<where>"
		+ "id = #{id}"
		+ "</where>"
		+ "</script>"
    )
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true)
    })
    Account findAccountTwo(@Param("id") int id);

    @Select("select id, name as name, money as money from account_two")
    List<Account> findAccountTwoList();
}
