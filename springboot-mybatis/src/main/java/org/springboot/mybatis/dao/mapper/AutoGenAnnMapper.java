package org.springboot.mybatis.dao.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springboot.mybatis.dao.entity.AutoGenAnn;
import org.springboot.mybatis.dao.entity.AutoGenAnnCriteria;

public interface AutoGenAnnMapper {
    @SelectProvider(type=AutoGenAnnSqlProvider.class, method="countByExample")
    long countByExample(AutoGenAnnCriteria example);

    @DeleteProvider(type=AutoGenAnnSqlProvider.class, method="deleteByExample")
    int deleteByExample(AutoGenAnnCriteria example);

    @Delete({
        "delete from auto_gen_ann",
        "where gen_id = #{genId,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer genId);

    @Insert({
        "insert into auto_gen_ann (gen_id, gen_name)",
        "values (#{genId,jdbcType=INTEGER}, #{genName,jdbcType=VARCHAR})"
    })
    int insert(AutoGenAnn record);

    @InsertProvider(type=AutoGenAnnSqlProvider.class, method="insertSelective")
    int insertSelective(AutoGenAnn record);

    @SelectProvider(type=AutoGenAnnSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="gen_id", property="genId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="gen_name", property="genName", jdbcType=JdbcType.VARCHAR)
    })
    List<AutoGenAnn> selectByExample(AutoGenAnnCriteria example);

    @Select({
        "select",
        "gen_id, gen_name",
        "from auto_gen_ann",
        "where gen_id = #{genId,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="gen_id", property="genId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="gen_name", property="genName", jdbcType=JdbcType.VARCHAR)
    })
    AutoGenAnn selectByPrimaryKey(Integer genId);

    @UpdateProvider(type=AutoGenAnnSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") AutoGenAnn record, @Param("example") AutoGenAnnCriteria example);

    @UpdateProvider(type=AutoGenAnnSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") AutoGenAnn record, @Param("example") AutoGenAnnCriteria example);

    @UpdateProvider(type=AutoGenAnnSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(AutoGenAnn record);

    @Update({
        "update auto_gen_ann",
        "set gen_name = #{genName,jdbcType=VARCHAR}",
        "where gen_id = #{genId,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(AutoGenAnn record);
}