package org.springboot.mybatis.dao.two.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springboot.mybatis.dao.entity.Account;

@Mapper
public interface IAccountTwoDAO {

	List<Account> selectAll();

}
