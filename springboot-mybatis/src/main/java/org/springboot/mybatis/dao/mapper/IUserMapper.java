package org.springboot.mybatis.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springboot.mybatis.dao.entity.User;

@Mapper
public interface IUserMapper {

	@Select("select * from user where name = #{name} and password = #{password}")
    User login(@Param("name") String name, @Param("password") String password);

}
