package org.springboot.mybatis.dao.entity;

public class AutoGenAnn {
    private Integer genId;

    private String genName;

    public Integer getGenId() {
        return genId;
    }

    public void setGenId(Integer genId) {
        this.genId = genId;
    }

    public String getGenName() {
        return genName;
    }

    public void setGenName(String genName) {
        this.genName = genName == null ? null : genName.trim();
    }
}