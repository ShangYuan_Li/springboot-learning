package org.springboot.mybatis.dao.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springboot.mybatis.dao.entity.AutoGenXml;
import org.springboot.mybatis.dao.entity.AutoGenXmlCriteria;

public interface AutoGenXmlMapper {
    long countByExample(AutoGenXmlCriteria example);

    int deleteByExample(AutoGenXmlCriteria example);

    int deleteByPrimaryKey(Integer genId);

    int insert(AutoGenXml record);

    int insertSelective(AutoGenXml record);

    List<AutoGenXml> selectByExample(AutoGenXmlCriteria example);

    AutoGenXml selectByPrimaryKey(Integer genId);

    int updateByExampleSelective(@Param("record") AutoGenXml record, @Param("example") AutoGenXmlCriteria example);

    int updateByExample(@Param("record") AutoGenXml record, @Param("example") AutoGenXmlCriteria example);

    int updateByPrimaryKeySelective(AutoGenXml record);

    int updateByPrimaryKey(AutoGenXml record);
}