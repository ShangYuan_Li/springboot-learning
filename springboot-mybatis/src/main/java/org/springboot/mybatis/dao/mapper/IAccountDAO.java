package org.springboot.mybatis.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springboot.mybatis.dao.entity.Account;

@Mapper
public interface IAccountDAO {

	List<Account> selectAll();

}
