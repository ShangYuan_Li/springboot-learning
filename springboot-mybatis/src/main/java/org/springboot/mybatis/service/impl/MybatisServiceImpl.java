package org.springboot.mybatis.service.impl;

import java.util.List;

import org.springboot.mybatis.dao.entity.Account;
import org.springboot.mybatis.dao.entity.AutoGenAnn;
import org.springboot.mybatis.dao.entity.AutoGenXml;
import org.springboot.mybatis.dao.mapper.AutoGenAnnMapper;
import org.springboot.mybatis.dao.mapper.AutoGenXmlMapper;
import org.springboot.mybatis.dao.mapper.IAccountDAO;
import org.springboot.mybatis.dao.mapper.IAccountMapper;
import org.springboot.mybatis.dao.two.mapper.IAccountTwoDAO;
import org.springboot.mybatis.dao.two.mapper.IAccountTwoMapper;
import org.springboot.mybatis.service.IMybatisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MybatisServiceImpl implements IMybatisService {

	@Autowired
    private IAccountDAO accountDao;
	
	@Autowired
    private IAccountTwoDAO accountTwoDao;
	
	@Autowired
    private AutoGenAnnMapper autoGenAnnMapper;
	
	@Autowired
    private AutoGenXmlMapper autoGenXmlMapper;
	
	@Autowired
    private IAccountMapper accountMapper;
	
	@Autowired
    private IAccountTwoMapper accountTwoMapper;

    public int add(String name, double money) {
        return accountMapper.add(name, money);
    }
    
    public int update(String name, double money, int id) {
        return accountMapper.update(name, money, id);
    }
    
    public int delete(int id) {
        return accountMapper.delete(id);
    }
    
    public Account findAccount(int id) {
        return accountMapper.findAccount(id);
    }
    
    public List<Account> findAccountList() {
        return accountMapper.findAccountList();
    }

	@Override
	public List<Account> selectAll() {
		return accountDao.selectAll();
	}

	@Override
	public List<AutoGenAnn> getAnnList() {
		return autoGenAnnMapper.selectByExample(null);
	}

	@Override
	public List<AutoGenXml> getXmlList() {
		return autoGenXmlMapper.selectByExample(null);
	}

	@Override
	public List<Account> findAccountTwoList() {		
		return accountTwoMapper.findAccountTwoList();
	}

	@Override
	public List<Account> selectAllTwo() {
		return accountTwoDao.selectAll();
	}

}
