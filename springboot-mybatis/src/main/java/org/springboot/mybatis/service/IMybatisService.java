package org.springboot.mybatis.service;

import java.util.List;

import org.springboot.mybatis.dao.entity.Account;
import org.springboot.mybatis.dao.entity.AutoGenAnn;
import org.springboot.mybatis.dao.entity.AutoGenXml;

public interface IMybatisService {

	List<Account> findAccountList();

	Account findAccount(int id);

	int update(String name, double money, int id);

	int delete(int id);

	int add(String name, double money);

	List<Account> selectAll();

	List<AutoGenAnn> getAnnList();

	List<AutoGenXml> getXmlList();

	List<Account> findAccountTwoList();

	List<Account> selectAllTwo();

}
