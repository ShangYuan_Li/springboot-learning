package org.springboot.jdbctemplate.service;

import java.util.List;

import org.springboot.jdbctemplate.dao.entity.Account;

public interface IAccountService {
	
	int add(Account account);

    int update(Account account);

    int delete(int id);

    Account findAccountById(int id);

    List<Account> findAccountList();

	void tx();
}
