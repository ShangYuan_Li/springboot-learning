package org.springboot.jdbctemplate.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import com.alibaba.druid.pool.DruidDataSource;

@Configuration
public class DatasourceConfig {

	@Value("${spring.datasource.jdbc.url}")
    private String url;
	@Value("${spring.datasource.jdbc.username}")
    private String username;
	@Value("${spring.datasource.jdbc.password}")
    private String password;
	@Value("${spring.datasource.jdbc.driver-class-name}")
    private String driverClassName;
	
	@Bean(name = "oneDataSource")
    DataSource oneDataSource() {
		DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setDriverClassName(driverClassName);
        return dataSource;
    }

	@Bean(name = "oneJdbcTemplate")
    public JdbcTemplate primaryJdbcTemplate(@Qualifier("oneDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}
