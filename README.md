# springboot-learning

#### 介绍
**springboot学习记录**
1. 搭建springboot工程(2.0.3)
2. 单元测试用例(Junit4)
3. springboot配置文件
4. 构建多模块化项目，集成JdbcTemplate数据访问Mysql, druid(1.2.8)
5. 集成JPA数据访问Mysql(8.0.25)
6. 集成Mybatis-注解和XML方式(2.2.0), druid(1.2.8)
7. 集成mongodb数据操作(5.0.3)
8. 集成redis单机版(win-3.2.100)
9. 文件下载-支持断点续传
10. 集成cache缓存
11. 集成Rabbitmq-direct,fanout,topic和普通队列(3.8.9)
12. 升级springboot版本(2.5.6)，单元测试用例(Junit5)
13. 集成定时任务Scheduled
14. 集成logback日志框架
15. 集成kafka(2.8.1)注解方式-集群(zookeeper-3.7.0)
16. 集成kafka(2.8.1)原始方式(apache)
17. 集成thyemeleaf(3.0.12.RELEASE)
18. 集成bootstrap(5.1.3) jquery(3.6.0)
19. 集成swagger2(3.0.0)
20. 集成elasticsearch(7.12.1)
21. 集成log4j2日志(2.14.1)
22. 集成JWT令牌(4.4.0)
23. 集成rocketmq(4.9.4)注解方式
24. 集成rocketmq(4.9.4)原始方式
25. spring事务
26. 集成aop-AspectJ框架
27. 集成liteflow(2.12.1)框架
28. spring事件发布监听

**启动**
1. mysql-3306
2. redis-6379
3. mongodb-27017
4. rabbitmq-15672
5. zookeeper-2181
6. kafka-9092、9093、9094
7. elasticsearch-9200
8. rocketmq-9876
