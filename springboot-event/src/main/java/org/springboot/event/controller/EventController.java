package org.springboot.event.controller;

import org.springboot.event.type.FirstEvent;
import org.springboot.event.type.SecondEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/event")
public class EventController {

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@RequestMapping(value = "/sendMsg")
    public String sendMsg() {
		FirstEvent firstEvent = new FirstEvent(this, "msg one");
        eventPublisher.publishEvent(firstEvent);
        
        SecondEvent secondEvent = new SecondEvent(this, "msg two");
        eventPublisher.publishEvent(secondEvent);
        return "complete !";
	}

}
