package org.springboot.event.type;

import org.springframework.context.ApplicationEvent;

public class CommonEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;
	
	private final String name;

    private final String msg;

	public CommonEvent(Object source, String name, String msg) {
		super(source);
		this.name = name;
		this.msg = msg;
	}

	public String getName() {
		return name;
	}

	public String getMsg() {
		return msg;
	}

}
