package org.springboot.event.type;

public class SecondEvent extends CommonEvent {

	private static final long serialVersionUID = 2L;

	public SecondEvent(Object source, String msg) {
		super(source, "SecondEvent", msg);
	}
}
