package org.springboot.event.type;

public class FirstEvent extends CommonEvent {

	private static final long serialVersionUID = 1L;

	public FirstEvent(Object source, String msg) {
		super(source, "FirstEvent", msg);
	}

}
