package org.springboot.event.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springboot.event.type.CommonEvent;
import org.springboot.event.type.FirstEvent;
import org.springboot.event.type.SecondEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ListenerEvent {

	static Logger logger = LoggerFactory.getLogger(ListenerEvent.class);

	@EventListener({FirstEvent.class, SecondEvent.class})
    public void eventListenerOne(CommonEvent event) {
		logger.info("eventListenerOne: {} {}", event.getName(), event.getMsg());
    }

	@EventListener({SecondEvent.class})
    public void eventListenerTwo(CommonEvent event) {
		logger.info("eventListenerTwo: {} {}", event.getName(), event.getMsg());
    }

	@EventListener
    public void eventListenerThree(CommonEvent event) {
		logger.info("eventListenerThree: {} {}", event.getName(), event.getMsg());
    }
	
	@EventListener
    public void eventListenerFour(ApplicationEvent event) {
		logger.info("eventListenerFour: {}", event);
    }

}
