package org.springboot.redis.controller;

import java.util.List;
import java.util.concurrent.TimeUnit;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

public class TransactionTest {
	
	public static void main(String[] args) {
		/**
		 * 特点:没有隔离级别(缓存到队列并未执行)，不保证原子性
		 * 注意:使用命令时，语法错误不影响后续命令执行和事务提交
		 */
		String type = "watch";
		
		if ("transaction".equals(type)) {
			jedisTransactionTest();
		} else if ("watch".equals(type)) {
			jedisWatchTest();
		}
	}

	public static void jedisTransactionTest() {
		Jedis jedis = new Jedis("127.0.0.1", 6379);
		// 开始事务
		Transaction t1 = jedis.multi();
		t1.set("husband", "Tom");
		t1.set("wife", "Mary");
		// 执行事务队列内命令
		t1.exec();
		
		Transaction t2 = jedis.multi();
		t2.set("test", "0");
		// 取消执行事务队列内命令
		t2.discard();
		
		System.out.println("husband:" + jedis.get("husband"));
		System.out.println("wife:" + jedis.get("wife"));
		System.out.println("test:" + jedis.get("test")); // null 原因：开启事务后未提交，则无结果
		jedis.close();
	}
	
	private static void jedisWatchTest() {
		Jedis jedis = new Jedis("127.0.0.1", 6379);
		jedis.set("watchkey", "goodboy");
		jedis.set("otherkey", "goodboy");
		// 当监听键发生变化时(可监听多个)，取消执行事务队列内命令
		jedis.watch("watchkey", "otherkey");
		Transaction multi = jedis.multi();
		new Thread(new Runnable() {
			@Override
			public void run() {
				Jedis jedis = new Jedis("127.0.0.1", 6379);
				Transaction multi = jedis.multi();
				multi.set("watchkey", "good"); 
				List<Object> exec = multi.exec();
				System.out.println("thread " + exec.get(0).toString()
						+ " watchkey:" + jedis.get("watchkey") + " otherkey:" + jedis.get("otherkey"));
				jedis.close();
			}
		}).start();
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		multi.set("watchkey", "boy");
		multi.set("otherkey", "boy");
		List<Object> list = multi.exec();
		System.out.println("main " + (list != null ? list.get(0).toString() : "NG")
				+ " watchkey:" + jedis.get("watchkey") + " otherkey:" + jedis.get("otherkey"));
		jedis.close();
	}
}
