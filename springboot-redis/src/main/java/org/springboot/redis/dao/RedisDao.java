package org.springboot.redis.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

@Repository
public class RedisDao {
	
	@Resource(name = "redisStrTemplate")
    private RedisTemplate<String, Object> redisTemplate;
	
    public void setKey(String key, String value){
        ValueOperations<String, Object> ops = redisTemplate.opsForValue();
        List<String> list = new ArrayList<String>();
        list.add(value);
        // 1分钟过期
        ops.set(key, list, 1, TimeUnit.MINUTES);
        ops.set(key + "'", value, 1, TimeUnit.MINUTES);
    }

    public String getValue(String key){
        ValueOperations<String, Object> ops = this.redisTemplate.opsForValue();
        return ops.get(key) == null ? null : ops.get(key).toString();
    }

}
