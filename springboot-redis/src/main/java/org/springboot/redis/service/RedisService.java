package org.springboot.redis.service;

public interface RedisService {

	void tmplSendMsg(String msg);

	void jedisSendMsg(String msg);

	void subscribe();

	void unsubscribe();
}
