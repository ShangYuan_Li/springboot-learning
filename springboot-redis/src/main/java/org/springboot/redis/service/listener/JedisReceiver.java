package org.springboot.redis.service.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import redis.clients.jedis.JedisPubSub;

/**
 * jedis 订阅消息监听
 * 
 * @author lisy
 */
@Component
public class JedisReceiver extends JedisPubSub {

	private static final Logger logger = LoggerFactory.getLogger(JedisReceiver.class);

	@Override
	public void onMessage(String channel, String message) {
		super.onMessage(channel, message);
		logger.info("jedis receice message={} ", message);
		logger.info("jedis receice channel={} ", channel);
	}

	/**
	 * 订阅了频道回调
	 */
	@Override
	public void onSubscribe(String channel, int subscribedChannels) {
		logger.info("jedis subscribe channel={} subscribedChannels={}", channel, subscribedChannels);
	}

	/**
	 * 取消订阅频道回调
	 */
	@Override
	public void onUnsubscribe(String channel, int subscribedChannels) {
		logger.info("jedis unsubscribe channel={} subscribedChannels={}", channel, subscribedChannels);
	}
}
