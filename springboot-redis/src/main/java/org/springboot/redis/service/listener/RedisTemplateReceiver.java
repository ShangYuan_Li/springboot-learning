package org.springboot.redis.service.listener;

import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;

/**
 * tmpl 订阅消息监听
 * 
 * @author lisy
 */
@Component
public class RedisTemplateReceiver implements MessageListener {

	private static final Logger logger = LoggerFactory.getLogger(RedisTemplateReceiver.class);
	
	@Override
    public void onMessage(Message message, byte[] pattern) {
        logger.info("template receice message={} ", new String(message.getBody(), StandardCharsets.UTF_8));
        logger.info("template receice channel={}", new String(pattern, StandardCharsets.UTF_8));
    }
	
}
