package org.springboot.redis.service.impl;

import javax.annotation.Resource;

import org.springboot.redis.config.JedisConfig;
import org.springboot.redis.service.RedisService;
import org.springboot.redis.service.listener.JedisReceiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import redis.clients.jedis.Jedis;

@Service
public class RedisServiceImpl implements RedisService {

	@Value("${redis.template.topic}")
    private String tmplTopic;
	
	@Value("${redis.jedis.topic}")
    private String jedisTopic;

	@Resource(name = "redisStrTemplate")
    private RedisTemplate<String, Object> redisTemplate;
	
	@Autowired
	private JedisReceiver jedisReceiver;

	@Override
	public void tmplSendMsg(String msg) {
		redisTemplate.convertAndSend(tmplTopic, msg);
	}

	@Override
	public void jedisSendMsg(String msg) {
		Jedis jedis = JedisConfig.getJedis();
		jedis.publish(jedisTopic, msg);
	}

	@Override
	public void subscribe() {
		Jedis jedis = JedisConfig.getJedis();
		jedis.subscribe(jedisReceiver, jedisTopic);
	}

	@Override
	public void unsubscribe() {
		jedisReceiver.unsubscribe(jedisTopic);
	}

}
