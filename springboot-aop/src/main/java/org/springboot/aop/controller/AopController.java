package org.springboot.aop.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aop")
public class AopController {

	/**
	 * 注意点
	 * 1、spring 需要启动 AspectJ 自动代理 @EnableAspectJAutoProxy，Spring Boot 的自动配置特性已经包含了这一设置
	 * 2、因为要在切面类中调用（目标类的）目标方法，所以目标方法要用 public 修饰才能被代理、调用，aop才会生效。
	 * 3、要直接通过目标类来调用目标方法，代理（aop）才会生效；如果是通过目标类的其它方法去调用目标方法（目标类内部调用目标方法），则代理（aop）不生效 
	 * 此种情况需要通过 AopContext 获取代理（目标类实例），通过代理来调用目标方法 (AopController) AopContext.currentProxy()
	 * 使用AopContext需要在启动类上配置允许暴露代理 @EnableAspectJAutoProxy(exposeProxy = true)
	 * 
	 * 正常执行流程：
	 *  - @Around 前置增强
	 *  - @Befor e前置通知
	 *  - 执行业务方法
	 *  - @AfterReturning 返回通知
	 *  - @After 后置通知
	 *  - @Around 后置增强
	 * 异常执行流程：
	 *  - @Around 前置增强
	 *  - @Befor e前置通知
	 *  - 执行业务方法
	 *  - @AfterThrowing 异常通知
	 *  - @After 后置通知
	 */
	
	@RequestMapping(value = "/aopMethod")
    public String aopMethod(String value) {
		int i = 1 / 1;
		return value + i;
	}

}
