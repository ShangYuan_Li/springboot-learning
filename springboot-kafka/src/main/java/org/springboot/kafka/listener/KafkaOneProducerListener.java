package org.springboot.kafka.listener;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class KafkaOneProducerListener implements ProducerListener<String, Object> {

	private static final Logger logger = LoggerFactory.getLogger(KafkaOneProducerListener.class);

	@Override
    public void onSuccess(ProducerRecord<String, Object> producerRecord, RecordMetadata recordMetadata) {
		logger.info("kafkaOne 消息发送通知 success, topic:{}, partition:{}, offset:{}, value:{}", recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset(), producerRecord.value());
    }

	@Override
    public void onError(ProducerRecord<String, Object> producerRecord, @Nullable RecordMetadata recordMetadata, Exception exception) {
        //可对进行重发重试
		logger.info("kafkaOne 消息发送通知 fail, topic:{}, partition:{}, offset:{}, value:{}", recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset(), producerRecord.value());
    }
	
}
