package org.springboot.kafka.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springboot.kafka.listener.KafkaOneProducerListener;
import org.springboot.kafka.listener.KafkaTwoProducerListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;

@Configuration
@EnableKafka // 开启 KafkaTemplate
public class KafkaConfig {

    @Value("${spring.kafka.one.bootstrap-servers}")
    private String bootstrapServers;
    @Value("${spring.kafka.one.consumer.group-id}")
    private String groupId;
    @Value("${spring.kafka.one.consumer.enable-auto-commit}")
    private boolean enableAutoCommit;
    
    @Autowired
    private KafkaOneProducerListener kafkaOneProducerListener;
 
    @Bean
    public KafkaTemplate<String, Object> kafkaOneTemplate() {
    	KafkaTemplate<String, Object> kafkaTemplate = new KafkaTemplate<>(producerFactory(bootstrapServers, groupId, enableAutoCommit));
    	kafkaTemplate.setProducerListener(kafkaOneProducerListener);
        return kafkaTemplate;
    }
    
    @Bean
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<Integer, String>> kafkaOneContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory(bootstrapServers, groupId, enableAutoCommit));
        factory.setConcurrency(3);
        factory.getContainerProperties().setPollTimeout(3000);
        return factory;
    }
    
    @Value("${spring.kafka.two.bootstrap-servers}")
    private String bootstrapServersTwo;
    @Value("${spring.kafka.two.consumer.group-id}")
    private String groupIdTwo;
    @Value("${spring.kafka.two.consumer.enable-auto-commit}")
    private boolean enableAutoCommitTwo;
    
    @Autowired
    private KafkaTwoProducerListener kafkaTwoProducerListener;
    
    @Bean
    public KafkaTemplate<String, Object> kafkaTwoTemplate() {
    	KafkaTemplate<String, Object> kafkaTemplate = new KafkaTemplate<>(producerFactory(bootstrapServersTwo, groupIdTwo, enableAutoCommitTwo));
    	kafkaTemplate.setProducerListener(kafkaTwoProducerListener);
    	return kafkaTemplate;
    }
    
    @Bean
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<Integer, String>> kafkaTwoContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory(bootstrapServersTwo, groupIdTwo, enableAutoCommitTwo));
        factory.setConcurrency(3);
        factory.getContainerProperties().setPollTimeout(3000);
        return factory;
    }

    private ProducerFactory<String, Object> producerFactory(String bootstrapServers, String groupId, boolean enableAutoCommit) {
        return new DefaultKafkaProducerFactory<>(producerConfigs(bootstrapServers, groupId, enableAutoCommit));
    }

    private Map<String, Object> producerConfigs(String bootstrapServers, String groupId, boolean enableAutoCommit) {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        props.put(ProducerConfig.ACKS_CONFIG, "1"); // 不能写成 1
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return props;
    }
    
    private ConsumerFactory<Integer, String> consumerFactory(String bootstrapServers, String groupId, boolean enableAutoCommit) {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(bootstrapServers, groupId, enableAutoCommit));
    }
 
    private Map<String, Object> consumerConfigs(String bootstrapServers, String groupId, boolean enableAutoCommit) {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, enableAutoCommit);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return props;
    }
}
