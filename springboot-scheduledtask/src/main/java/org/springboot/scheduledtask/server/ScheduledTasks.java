package org.springboot.scheduledtask.server;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    /** 
     * fixedRate = 5000：上一次开始执行时间点之后5秒再执行
     * fixedDelay = 5000：上一次执行完毕时间点之后5秒再执行
     * initialDelay=1000，fixedRate=5000：第一次延迟1秒后执行，之后按fixedRate的规则每5秒执行一次
     * cron="/5"：通过cron表达式定义规则
     * 	- 表达式：秒 分 时 天 月 周 年
     *		Seconds(秒)：可以用数字0－59 表示
     *		Minutes(分)：可以用数字0－59 表示
     *		Hours(时)：可以用数字0-23表示
     *		Day-of-Month(天)：可以用数字1-31 中的任一一个值，但要注意一些特别的月份
     *		Month(月)：可以用0-11 或用字符串"JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV and DEC"表示
     *		Day-of-Week(每周)：可以用数字1-7表示(1=星期日)或用字符口串"SUN, MON, TUE, WED, THU, FRI and SAT"表示
     * 	- 符号：
     * 		* - 代表整个时间段
     * 		, - 指定数个值
     * 		- - 指定一个值的范围
     * 		/ - 为特别单位，表示为"每"如"0/15"表示每隔15分钟执行一次, 从0分开始计算
     * 		? - 表示每月的某一天，或第周的某一天
     * 		L - 用于每月，或每周，表示为每月的最后一天
     * 		W - 表示为最近工作日,如"15W"放在每月（day-of-month）字段上表示为"到本月15日最近的工作日" 
     * 		# - 是用来指定"的"每月第n个工作日,例 在每周（day-of-week）这个字段中内容为"6#3"or"FRI#3"则表示"每月第三个星期五" 
     */
    @Scheduled(cron="0 0/1 * * * ?")
    public void reportCurrentTime() {
    	log.trace("The time is now {}", dateFormat.format(new Date()));
    	log.debug("The time is now {}", dateFormat.format(new Date()));
        log.info("The time is now {}", dateFormat.format(new Date()));
        log.warn("The time is now {}", dateFormat.format(new Date()));
        log.error("The time is now {}", dateFormat.format(new Date()));
    }
}
