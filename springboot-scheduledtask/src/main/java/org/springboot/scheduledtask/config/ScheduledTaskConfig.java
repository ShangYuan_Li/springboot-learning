package org.springboot.scheduledtask.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 开启定时任务
 * 
 * @author lisy
 */
@Configuration
@EnableScheduling
public class ScheduledTaskConfig {

	/**
	 * Scheduled task有三种使用的方式
	 * 	- 在注解上直接使用task定时任务
	 * 	- 更改定时任务的时间
	 * 	- 手动启动定时任务和停止定时任务以及更改定时任务的时间
	 * 缺点：不适用于分布式集群
	 */
	
}
