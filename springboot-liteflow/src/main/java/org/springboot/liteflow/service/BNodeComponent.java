package org.springboot.liteflow.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.yomahub.liteflow.core.NodeComponent;

@Component("b")
public class BNodeComponent extends NodeComponent {
	
	static Logger logger = LoggerFactory.getLogger(BNodeComponent.class);

	@Override
	public void process() {
		logger.info("b start!");
	}
}

