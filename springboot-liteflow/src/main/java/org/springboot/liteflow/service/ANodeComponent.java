package org.springboot.liteflow.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.yomahub.liteflow.core.NodeComponent;

@Component("a")
public class ANodeComponent extends NodeComponent {

	static Logger logger = LoggerFactory.getLogger(ANodeComponent.class);
	
	@Override
	public void process() {
		logger.info("a start!");
	}
}

