package org.springboot.liteflow.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.yomahub.liteflow.core.NodeComponent;

@Component("c")
public class CNodeComponent extends NodeComponent {
	
	static Logger logger = LoggerFactory.getLogger(CNodeComponent.class);

	@Override
	public void process() {
		logger.info("c start!");
	}
}

