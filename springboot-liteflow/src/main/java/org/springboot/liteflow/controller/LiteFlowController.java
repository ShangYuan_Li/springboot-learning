package org.springboot.liteflow.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;

@RestController
@RequestMapping("/liteflow")
public class LiteFlowController {
	
	static Logger logger = LoggerFactory.getLogger(LiteFlowController.class);

	@Resource
    private FlowExecutor flowExecutor;
	
	@RequestMapping(value = "/chain")
    public Boolean chain(String name) {
		LiteflowResponse response = flowExecutor.execute2Resp(name, "arg");
		logger.info("{} end {}", response.getChainId(), response.getSlot().getRequestData());
		return response.isSuccess();
	}
	
}
