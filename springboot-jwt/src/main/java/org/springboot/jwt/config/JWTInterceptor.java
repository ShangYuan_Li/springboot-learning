package org.springboot.jwt.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springboot.jwt.util.JwtUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * JWTInterceptor 是一个拦截器，用于验证请求头中的 JWT 令牌是否有效。
 * 当有请求进入时，该拦截器会首先从请求头中获取令牌，并尝试验证其有效性。
 * 如果令牌验证成功，则放行请求；否则，拦截请求并返回相应的错误信息。
 */
public class JWTInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 创建一个 Map 对象，用于存储响应信息
        Map<String, Object> map = new HashMap<>();
        // 从请求头中获取令牌
        String token = request.getHeader("token");
        try {
        	// 验证令牌的有效性
        	JwtUtils.verify(token);
            return true;
        } catch (SignatureVerificationException e) {
            e.printStackTrace();
            map.put("msg", "无效签名!");
        } catch (TokenExpiredException e) {
            e.printStackTrace();
            map.put("msg", "token过期!");
        } catch (AlgorithmMismatchException e) {
            e.printStackTrace();
            map.put("msg", "token算法不一致!");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("msg", "token无效!!");
        }
        // 设置状态为 false
        map.put("state", false); 
        // 将 Map 转化为 JSON 字符串（使用 Jackson 库）
        String json = new ObjectMapper().writeValueAsString(map);
        // 设置响应的 Content-Type
        response.setContentType("application/json;charset=UTF-8");
        // 将 JSON 字符串写入响应中
        response.getWriter().println(json);
        return false;
    }

}

