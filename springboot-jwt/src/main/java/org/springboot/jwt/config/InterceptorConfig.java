package org.springboot.jwt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * InterceptorConfig 是一个配置类，用于添加拦截器。
 * 在这个类中，我们可以配置需要拦截的接口路径以及排除不需要拦截的接口路径。
 * 在这个例子中，我们添加了 JWTInterceptor 拦截器来对请求进行 token 验证，
 * 并设置了"/jwt/test"、"/mybatis/list"接口需要进行验证，而"/jwt/login"接口则被排除在验证之外，即所有用户都放行登录接口。
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

	/**
     * 添加 JWT 拦截器配置
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    	InterceptorRegistration interceptor = registry.addInterceptor(new JWTInterceptor());
    	// 对"/user/test"接口进行token验证
    	interceptor.addPathPatterns("/jwt/test");
    	interceptor.addPathPatterns("/mybatis/list");
    	// 所有用户都放行登录接口
    	interceptor.excludePathPatterns("/jwt/login");
    }
}
