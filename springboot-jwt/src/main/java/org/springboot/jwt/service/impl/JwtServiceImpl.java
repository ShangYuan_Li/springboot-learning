package org.springboot.jwt.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springboot.jwt.service.IJwtService;
import org.springboot.mybatis.dao.entity.User;
import org.springboot.mybatis.dao.mapper.IUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JwtServiceImpl implements IJwtService {

	private static final Logger logger = LoggerFactory.getLogger(JwtServiceImpl.class);
	
	@Autowired
	private IUserMapper userMapper;
	
	@Override
	public User login(User user) {
		User userDB = userMapper.login(user.getName(), user.getPassword());
        logger.info("user: [{}]", userDB);
        if (userDB != null){
            return userDB;
        }
        throw new RuntimeException("登录失败~~");
	}

}
