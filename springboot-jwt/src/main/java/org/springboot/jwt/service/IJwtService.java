package org.springboot.jwt.service;

import org.springboot.mybatis.dao.entity.User;

public interface IJwtService {

	User login(User user);

}
