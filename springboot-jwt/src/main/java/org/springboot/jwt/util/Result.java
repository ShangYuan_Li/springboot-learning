package org.springboot.jwt.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {

    private int code;
    private String message;
    private T data;

    public Result(T data) {
        this.code = 200;
        this.message = "success";
        this.data = data;
    }

    public Result(T data, boolean success, String message) {
        if (success) {
            this.code = 200;
            this.message = "success";
        } else {
            this.code = 500; // 自定义错误状态码（示例为500）
            this.message = message;
        }
        this.data = data;
    }

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
        this.data = null;
    }

    /**
     * 返回执行失败的结果（默认状态码为500）
     *
     * @param message 提示信息
     * @return 失败的结果对象
     */
    public static <T> Result<T> fail(String message) {
        return new Result<>(500, message);
    }

    /**
     * 返回执行失败的结果（自定义状态码和提示信息）
     *
     * @param code    状态码
     * @param message 提示信息
     * @return 失败的结果对象
     */
    public static <T> Result<T> fail(int code, String message) {
        return new Result<>(code, message);
    }
}

