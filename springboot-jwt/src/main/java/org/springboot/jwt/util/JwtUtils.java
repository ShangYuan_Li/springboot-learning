package org.springboot.jwt.util;

import java.util.Calendar;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

/**
 * token 工具类
 * 
 * @author lisy
 */
public class JwtUtils {

	/**
	 * 定义密钥
	 */
	private static String SIGN = "token!Q@W3e4r"; 

	/**
	 * 生成 JWT 令牌
	 * @param username
	 * @return
	 */
    public static String getToken(String username) {
    	JWTCreator.Builder builder = JWT.create();
    	// Payload 可以存放业务信息
        builder.withClaim("username", username);
        // 设置 90 秒超时
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.SECOND, 90);
        builder.withExpiresAt(instance.getTime());
        // 使用 HMAC256 签名算法进行签名，并返回令牌字符串
        return builder.sign(Algorithm.HMAC256(SIGN));
    }
    
    /**
     * 验证 JWT 令牌，获取令牌中的 Payload 数据
     * @param token 要解析的令牌字符串
     * @return 解码后的令牌对象（DecodedJWT）
     */
    public static DecodedJWT verify(String token){
        // 创建一个 JWTVerifier 实例，使用相同的密钥构建，并对令牌进行验证和解码
        return JWT.require(Algorithm.HMAC256(SIGN)).build().verify(token);
    }

}
