package org.springboot.server.controller;

import org.springboot.server.bean.ConfigBean;
import org.springboot.server.bean.ConfigTestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableConfigurationProperties({ConfigBean.class, ConfigTestBean.class})
public class ConfigController {
	
	@Value("${my.name}")
    private String name;
    @Value("${my.age}")
    private int age;
	
	@Autowired
    ConfigBean configBean;
	@Autowired
    ConfigTestBean configTestBean;
	
    @RequestMapping(value = "/configValue")
    public String configValue(){
        return name + " : " + age;
    }

    @RequestMapping(value = "/configBean")
    public String configBean(){
        return configBean.getGreeting() + " >>>> " + configBean.getAge() + " >>>> " + configBean.getUuid() + " >>>> " + configBean.getMax();
    }
    
    @RequestMapping(value = "/configTestBean")
    public String configTestBean(){
        return configTestBean.getName() + " " + configTestBean.getAge();
    }
}
