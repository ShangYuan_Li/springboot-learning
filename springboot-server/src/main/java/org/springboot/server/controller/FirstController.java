package org.springboot.server.controller;

import org.springboot.server.bean.InitTestBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {

	@Bean(initMethod = "initMethod")
    public InitTestBean initTestBean() {
        return new InitTestBean();
    }

	@RequestMapping("/")
    public String Hello() {
        return "Greetings from Spring Boot!";
    }
}
