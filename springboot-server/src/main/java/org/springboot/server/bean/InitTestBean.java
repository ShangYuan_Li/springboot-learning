package org.springboot.server.bean;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.InitializingBean;

public class InitTestBean implements InitializingBean {

	private String message;

    public void setMessage(String message) {
        this.message = message;
    }
    
    /**
     * 执行顺序 @PostConstruct -> afterPropertiesSet -> initMethod
     * 在实现 afterPropertiesSet 方法时，一定不要抛出异常
     */

    @PostConstruct
    public void init() {
        System.out.println("TestBean @PostConstruct, message =  " + message);
    }

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("TestBean afterPropertiesSet, message = " + message);
	}

	public void initMethod() {
		 System.out.println("TestBean init-method, message =  " + message);
    }
	
}
