package org.springboot.server.controller;

import java.net.URL;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

/**
 * Junit5测试用例
 * 
 * @author lisy
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//下面两个注解是等价的，由于@SpringBootTest注解包含了@ExtendWith注解，所以使用Junit5下面两个注解可以不用加。
//@ExtendWith({SpringExtension.class})
//@SpringJUnitConfig
public class FirstControllerTest2{

	@LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @BeforeEach
    public void setUp() throws Exception {
    	System.out.println("test: http://localhost:" + port + "/");
        this.base = new URL("http://localhost:" + port + "/");
    }
    
	@Test
	public void testHello() {
		ResponseEntity<String> response = template.getForEntity(base.toString(), String.class);
		System.out.println("test: " + response.getBody());
	}
}