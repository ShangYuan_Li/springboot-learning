package org.springboot.cache.dao;

import org.springboot.cache.entity.Book;

public interface IBookRepository {

	Book getByIsbn(String isbn);
}
