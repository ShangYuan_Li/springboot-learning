package org.springboot.cache.controller;

import org.springboot.cache.dao.IBookRepository;
import org.springboot.cache.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * cache缓存
 * 
 * @author lisy
 */
@RestController
@RequestMapping("/cache")
public class CacheController {
	
	@Autowired
	IBookRepository bookRepository;
	
	@RequestMapping(value = "/getByIsbn", method = RequestMethod.GET)
    public Book getByIsbn() {
		Book book = bookRepository.getByIsbn("isbn-5678");
		return book;
	}
	
}
