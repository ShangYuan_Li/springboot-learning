package org.springboot.cache.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;

@Component
@EnableCaching
public class CacheConfig {

}
