package org.springboot.mongodb.config;

import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Component;

@Component
@EnableMongoRepositories(basePackages={"org.springboot.mongodb.dao"})
public class MongoDBConfig {

}
