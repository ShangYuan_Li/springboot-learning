package org.springboot.mongodb.controller;

import java.util.List;

import org.springboot.mongodb.dao.CustomerRepository;
import org.springboot.mongodb.dao.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mongodb")
public class MongoDBController {

	@Autowired
	private CustomerRepository repository;
	
	@RequestMapping(value = "/save", method = RequestMethod.GET)
    public Customer save(String firstName, String lastName) {
        return repository.save(new Customer(firstName, lastName));
    }
	
	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public List<Customer> findAll() {
        return repository.findAll();
    }
	
	@RequestMapping(value = "/findByFirstName", method = RequestMethod.GET)
    public Customer findByFirstName(String firstName) {
        return repository.findByFirstName(firstName);
    }
	
	@RequestMapping(value = "/findByLastName", method = RequestMethod.GET)
    public List<Customer> findByLastName(String lastName) {
        return repository.findByLastName(lastName);
    }
	
}
