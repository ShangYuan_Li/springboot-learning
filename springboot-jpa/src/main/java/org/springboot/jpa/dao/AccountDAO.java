package org.springboot.jpa.dao;

import org.springboot.jpa.dao.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountDAO extends JpaRepository<Account,Integer>{

}
