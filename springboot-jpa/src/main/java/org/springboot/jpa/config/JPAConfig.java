package org.springboot.jpa.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(value = "org.springboot.jpa.dao")
@EntityScan(basePackages = "org.springboot.jpa.dao.entity")
public class JPAConfig {

	@Value("${spring.datasource.url}")
	private String url;

	@Value("${spring.datasource.username}")
	private String user;

	@Value("${spring.datasource.password}")
	private String password;

	@Value("${spring.datasource.driver-class-name}")
	private String driverClass;

	@Bean(name = "primaryDataSource")
	@Primary
	public DataSource primaryDataSource() {
		return DataSourceBuilder.create().driverClassName(driverClass).url(url).username(user).password(password)
				.build();
	}
}
